/* socket.c: Simple Socket Functions */

#include "chippewa.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

/**
 * Allocate socket, bind it, and listen to specified port.
 **/
int
socket_listen(const char *port)
{
    struct addrinfo  hints;
    struct addrinfo *results;
    int    socket_fd = -1;

    /* Lookup server address information */
    /* TODO */

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	/* Allocate socket */
        /* TODO */

	/* Bind socket */
        /* TODO */

    	/* Listen to socket */
        /* TODO */

	goto success;
    }

    socket_fd = -1;

success:
    freeaddrinfo(results);
    return (socket_fd);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */

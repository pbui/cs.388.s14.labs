/* echo_client: Simple Echo Client */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...)	fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define ECHO_PORT   "9911"

FILE * socket_dial(const char *host, const char *port);

int
main(int argc, char *argv[])
{
    char *host;
    char *port;
    char buffer[BUFSIZ];
    FILE *csock;

    if (argc != 3) {
    	fprintf(stderr, "usage: echo_client host port\n");
    	exit(EXIT_FAILURE);
    }

    /* Connect to server */
    host  = argv[1];
    port  = argv[2];
    csock = socket_dial(host, port);
    if (csock == NULL) {
    	fprintf(stderr, "Unable to connect to %s:%s: %s\n", host, port, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    /* Send stdin to server and write results to stdout */
    while (true) {
    	printf("> ");
    	fflush(stdout);

	/* Read from stdin */
    	if (fgets(buffer, BUFSIZ, stdin) == NULL) {
    	    break;
	}

	/* Write to to server */
	if (fputs(buffer, csock) == EOF) {
	    break;
	}
    	
	/* Read from server */
    	if (fgets(buffer, BUFSIZ, csock) == NULL) {
    	    break;
	}

	/* Write to stdout */
	fputs(buffer, stdout);
    }

    fclose(csock);
    return (EXIT_SUCCESS);
}

FILE *
socket_dial(const char *host, const char *port)
{
    struct addrinfo   *results;
    struct addrinfo    hints;
    int    socket_fd = -1;

    /* Lookup server address information */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if (getaddrinfo(host, port, &hints, &results) < 0) {
	return (NULL);
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    debug("Unable to make socket: %s", strerror(errno));
	    continue;
	}

	/* Connect to host */
	if (connect(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    debug("Unable to connect: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

	break;
    }
    freeaddrinfo(results);

    /* Open and return FILE from socket */
    return fdopen(socket_fd, "r+");
}

/* echo_server: Simple Echo Server */

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...)	fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define MAX(a, b)   ((a) > (b) ? (a) : (b))

#define ECHO_PORT   "9911"

struct echo_client_t {
    int   fd;
    FILE *socket;
    char  host[NI_MAXHOST];
    char  port[NI_MAXSERV];

    TAILQ_ENTRY(echo_client_t) clients;
};

TAILQ_HEAD(echo_clients_t, echo_client_t);

int  socket_listen(const char *port);
void compute_rfds(fd_set *rfds, int *nfds, int sfd, struct echo_clients_t *cs);
int  accept_client(int sfd, struct echo_clients_t *cs);
int  handle_client(struct echo_client_t *c);

int
main(int argc, char *argv[])
{
    char *port;
    int  sfd;
    int  nfds;
    fd_set rfds;
    struct echo_clients_t cs;
    struct timeval timeout;

    /* Parse command line options */
    if (argc != 2) {
    	fprintf(stderr, "usage: echo_server port\n");
    	exit(EXIT_FAILURE);
    }

    port = argv[1];
    sfd  = socket_listen(port);
    if (sfd < 0) {
    	fprintf(stderr, "Unable to listen on %s: %s\n", port, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    TAILQ_INIT(&cs);

    /* Accept and handle incoming connections */
    while (true) {
    	/* Compute FDSet */
    	compute_rfds(&rfds, &nfds, sfd, &cs);

	/* Set timeout */
	timeout.tv_sec  = 1;
	timeout.tv_usec = 0;

	/* Poll file descriptors for reading */
    	if (select(nfds + 1, &rfds, NULL, NULL, &timeout) < 0) {
    	    fprintf(stderr, "Error during select: %s\n", strerror(errno));
    	    continue;
	}

	/* Check server socket */
	if (FD_ISSET(sfd, &rfds)) {
	    /* Accept client */
	    if (accept_client(sfd, &cs) < 0) {
		fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
		continue;
	    }
	}
	
	/* Check client sockets */
	struct echo_client_t *c;
	struct echo_client_t *next;

	for (c = TAILQ_FIRST(&cs); c; c = next) {
	    next = TAILQ_NEXT(c, clients);
	    if (FD_ISSET(c->fd, &rfds)) {
	    	/* Handle client: remove from list if disconnected */
	    	if (handle_client(c) < 0) {
	    	    printf("[%s] disconnected\n", c->host);
	    	    TAILQ_REMOVE(&cs, c, clients);
	    	    free(c);
		}
	    }
	}
    }

    close(sfd);
    return (EXIT_SUCCESS);
}

int
socket_listen(const char *port)
{
    struct addrinfo  hints;
    struct addrinfo *results;
    int    socket_fd = -1;

    /* Lookup server address information */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;   /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* Use TCP */
    hints.ai_flags    = AI_PASSIVE;  /* Use all interfaces */

    if (getaddrinfo(NULL, port, &hints, &results) < 0) {
	return (-1);
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    debug("Unable to make socket: %s", strerror(errno));
	    continue;
	}

	/* Bind socket */
	if (bind(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    debug("Unable to bind: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

    	/* Listen to socket */
	if (listen(socket_fd, SOMAXCONN) < 0) {
	    debug("Unable to listen: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

	goto success;
    }

    socket_fd = -1;

success:
    freeaddrinfo(results);
    return (socket_fd);
}

void
compute_rfds(fd_set *rfds, int *nfds, int sfd, struct echo_clients_t *cs)
{
    struct echo_client_t *c;

    /* Clear FDSet */
    FD_ZERO(rfds);

    /* Add sockets to FDSet */
    *nfds = sfd;
    FD_SET(sfd, rfds);
    TAILQ_FOREACH(c, cs, clients) {
    	FD_SET(c->fd, rfds);
    	if (*nfds < c->fd) {
    	    *nfds = c->fd;
	}
    }
}

int
accept_client(int sfd, struct echo_clients_t *cs)
{
    struct echo_client_t *c;
    struct sockaddr caddr;
    socklen_t clen;

    /* Allocate echo client struct */
    c = calloc(sizeof(struct echo_client_t), 1);
    if (c == NULL) {
    	fprintf(stderr, "Unable to allocate echo client struct: %s\n", strerror(errno));
    	return (-1);
    }

    /* Accept a client */
    clen  = sizeof(caddr);
    c->fd = accept(sfd, &caddr, &clen);
    if (c->fd < 0) {
	fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
	return (-1);
    }

    /* Lookup client information */
    if (getnameinfo(&caddr, clen, c->host, sizeof(c->host), c->port, sizeof(c->port), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
	fprintf(stderr, "Unable to lookup client: %s\n", strerror(errno));
	close(c->fd);
	return (-1);
    }
    
    /* Open socket stream */
    c->socket = fdopen(c->fd, "r+");
    if (c->socket < 0) {
	fprintf(stderr, "Unable to open file descriptor: %s\n", strerror(errno));
	_exit(EXIT_FAILURE);
    }

    /* Append echo client to list */
    TAILQ_INSERT_TAIL(cs, c, clients);

    printf("[%s] connected on %s\n", c->host, c->port);
    return (0);
}

int
handle_client(struct echo_client_t *c)
{
    char buffer[BUFSIZ];

    /* Read from client */
    if (fgets(buffer, BUFSIZ, c->socket) == NULL) {
	return (-1);
    }

    /* Log to stdout */
    printf("[%s] %s", c->host, buffer);

    /* Echo to client */
    if (fputs(buffer, c->socket) == EOF) {
	return (-1);
    }

    fflush(c->socket);
    return (0);
}

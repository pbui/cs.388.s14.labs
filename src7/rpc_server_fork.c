/* rpc_server: Simple RPC Server */

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...)	fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define chomp(s)   (s)[strlen(s) - 1] = '\0'

#define RPC_PORT   "9911"

struct rpc_client_t {
    int   fd;
    FILE *socket;
    char  host[NI_MAXHOST];
    char  port[NI_MAXSERV];
};

int  socket_listen(const char *port);
int  accept_client(int sfd, struct rpc_client_t *c);
void handle_client(struct rpc_client_t *c);

int
main(int argc, char *argv[])
{
    char *port;
    int  sfd;

    /* Parse command line options */
    if (argc != 2) {
    	fprintf(stderr, "usage: echo_server port\n");
    	exit(EXIT_FAILURE);
    }

    port = argv[1];
    sfd  = socket_listen(port);
    if (sfd < 0) {
    	fprintf(stderr, "Unable to listen on %s: %s\n", port, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    /* Ignore children */
    signal(SIGCHLD, SIG_IGN);

    /* Accept and handle incoming connections */
    while (true) {
    	struct rpc_client_t c;
    	pid_t cpid;

	/* Accept client */
    	if (accept_client(sfd, &c) < 0) {
	    fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
	    continue;
	}

	/* Handle client */
	cpid = fork();
	switch (cpid) {
	case -1:
	    fprintf(stderr, "Unable to fork: %s\n", strerror(errno));
	    break;
	case  0:
	    close(sfd);		/* Close server FD */
	    handle_client(&c);
	    break;
	default:
	    close(c.fd);	/* Close client FD */
	    break;
	}
    }

    close(sfd);
    return (EXIT_SUCCESS);
}

int
socket_listen(const char *port)
{
    struct addrinfo  hints;
    struct addrinfo *results;
    int    socket_fd = -1;

    /* Lookup server address information */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;   /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* Use TCP */
    hints.ai_flags    = AI_PASSIVE;  /* Use all interfaces */

    if (getaddrinfo(NULL, port, &hints, &results) < 0) {
	return (-1);
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    debug("Unable to make socket: %s", strerror(errno));
	    continue;
	}

	/* Bind socket */
	if (bind(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    debug("Unable to bind: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

    	/* Listen to socket */
	if (listen(socket_fd, SOMAXCONN) < 0) {
	    debug("Unable to listen: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

	goto success;
    }

    socket_fd = -1;

success:
    freeaddrinfo(results);
    return (socket_fd);
}

int
accept_client(int sfd, struct rpc_client_t *c)
{
    struct sockaddr caddr;
    socklen_t clen;

    /* Clear struct echo client struct */
    memset(c, 0, sizeof(struct rpc_client_t));

    /* Accept a client */
    clen  = sizeof(caddr);
    c->fd = accept(sfd, &caddr, &clen);
    if (c->fd < 0) {
	fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
	return (-1);
    }

    /* Lookup client information */
    if (getnameinfo(&caddr, clen, c->host, sizeof(c->host), c->port, sizeof(c->port), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
	fprintf(stderr, "Unable to lookup client: %s\n", strerror(errno));
	close(c->fd);
	return (-1);
    }

    printf("[%s] connected on %s\n", c->host, c->port);
    return (0);
}

void
handle_client(struct rpc_client_t *c)
{
    char buffer[BUFSIZ];

    /* Open socket stream */
    c->socket = fdopen(c->fd, "r+");
    if (c->socket < 0) {
	fprintf(stderr, "Unable to open file descriptor: %s\n", strerror(errno));
	_exit(EXIT_FAILURE);
    }

    while (true) {
    	FILE *pfs;

	/* Read from client */
	if (fgets(buffer, BUFSIZ, c->socket) == NULL) {
	    break;
	}

	chomp(buffer);

	/* Log to stdout */
	printf("[%s] %s\n", c->host, buffer);

	/* Check RPC command and open process */
	if (strchr(buffer, ';') || strchr(buffer, '&') || strchr(buffer, '|')) {
	    fprintf(c->socket, "Invalid RPC: %s\n", buffer);
	    continue;
	}

	if (strncmp(buffer, "date", 4) == 0) {
	    pfs = popen(buffer, "r");
	} else if (strncmp(buffer, "uname", 5) == 0) {
	    pfs = popen(buffer, "r");
	} else if (strncmp(buffer, "cal", 3) == 0) {
	    pfs = popen(buffer, "r");
	} else {
	    fprintf(c->socket, "Invalid RPC: %s\n", buffer);
	    continue;
	}

	if (pfs == NULL) {
	    fprintf(c->socket, "Could not popen %s: %s\n", buffer, strerror(errno));
	    continue;
	}

	/* Send process output to client socket */
	while (fgets(buffer, BUFSIZ, pfs)) {
	    if (fputs(buffer, c->socket) == EOF) {
	    	break;
	    }
	}

	pclose(pfs);
    }

    printf("[%s] disconnected\n", c->host);
    fclose(c->socket);
    _exit(EXIT_SUCCESS);
}
